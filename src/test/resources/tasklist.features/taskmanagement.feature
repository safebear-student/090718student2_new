Feature: Task Management

  User Story:
  In order to manage my tasks
  As a user
  I must be able to view add, archive or update dates

  Rules:
   -You must be able to view all tasks
  - They must be paginated
  - You must be able to see the name, due date and status of a task
  - You must be able to change the status of a task
  - You must be able to archive tasks

  Questions:
  - How many tasks per page for the pagination?
  -Where do you archive to?
  -Is the due date configurable or should it be hard coded to a certain number of days in the future

  To do:
  -Pagination
  -Archiving

  Domain language:
  - Task = this is made up of a description, a status and a due date
  - Due date = this must be in the format dd/mm/yyyy
  - Task list = this is a list of tasks and can be displayed on the home page
 #this is a test

#  Background:
#    Given the following tasks are created
#    |Configure Jenkins|
#    |Setup automation environment|
#    |Setup SQL Server |
@to-do
    Scenario: A user opens the home page
      When the home page opens
      Then the following tasks appear in the list :
        |Configure Jenkins|
        |Setup automation environment|
        |Setup SQL Server |

 @to-do
      Scenario Outline: A user changes the state of a task
        Given a <task> is in a <startingstate>
        When a <task> is updated to <endstate>
        Then the <task> is now in <endstate>
        Examples:
          |startingstate| task| endstate|
          |notcompleted | ConfigureJenkins | completed |


  Scenario Outline: A user creates a task
    When a user creates a <task>
    Then the <task> appears in the list
    Examples:
      |task|
      |cooking|
      |gardening|

