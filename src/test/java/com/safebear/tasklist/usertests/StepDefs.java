package com.safebear.tasklist.usertests;

import com.safebear.tasklist.usertests.pages.TaskListPage;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import org.assertj.core.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by CCA_Student on 09/07/2018.
 * everything is awesome
 */
public class StepDefs {

    // Setup
    final String DOMAIN = System.getProperty("domain");
    final String PORT = System.getProperty("port");
    final String CONTEXT= System.getProperty("context");

    WebDriver driver;

    TaskListPage taskListPage;

    @Before
    public void setUp(){

        String browser= System.getProperty("browser");

        switch(browser) {
            case "headless":
                ChromeOptions options = new ChromeOptions();
                //Two versions of this: https://dzone.com/articles/running-selenium-tests-with-chrome-headless  and
                // https://thefriendlytester.co.uk/2017/04/new-headless-chrome-with-selenium.html (with a without -- prefix)
                options.addArguments("--headless");
                driver = new ChromeDriver(options);
                break;
            case "chrome":
                driver = new ChromeDriver();
                break;
            default:
                driver = new ChromeDriver();
                break;
        }
        driver.manage().deleteAllCookies();

        taskListPage = new TaskListPage(driver);
        String url = DOMAIN + ":" + PORT + "/" + CONTEXT;
        driver.get(url);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    @After
    public void tearDown() {

        try {
            Thread.sleep(2000);

        }catch (InterruptedException e){
            e.printStackTrace();
        }

        driver.quit();
    }



    @When("^a user creates a (.+)$")
    public void a_user_creates_a_task(String taskname) {
        // Write code here that turns the phrase above into concrete actions
        taskListPage.addTask(taskname);
    }

    @Then("^the (.+) appears in the list$")
    public void the_task_appears_in_the_list(String taskname) {
        // Write code here that turns the phrase above into concrete actions
        Assertions.assertThat(taskListPage.checkForTask(taskname)).isTrue();


    }


    //New stuff
     // No bind @Given("^the following (?:Configure Jenkins|Setup automation environment||Setup SQL Server) are created$")
    @Given("^the following tasks are created$")
    public void the_following_tasks_are_created(DataTable tasks) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)
        throw new PendingException();
    }

    @When("^the home page opens$")
    public void the_home_page_opens() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

        //@Then("^the following (?:'Configure Jenkins'|'Setup automation environment'||'Setup SQL Server') appear in the list:$")
    @Then("^the following tasks appear in the list :$")
    public void the_following_tasks_appear_in_the_list(DataTable tasks) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)
        throw new PendingException();
    }

    @Given("^a (.+) is in a (.+)$")
    public void a_task_is_in_a_state(String taskname, String status) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^a (.+) is updated to (.+)$")
    public void a_task_is_updated_to_newstate(String taskname, String newstate) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^the (.+) is now in (.+)")
    public void the_task_is_now_in_newstate(String taskname, String newstate) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}
