package com.safebear.tasklist.usertests;

import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

import java.io.File;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"com.cucumber.listener.ExtentCucumberFormatter:target/cucumber/extent_report.html"},
        tags = "~@to-do",
        glue = "com.safebear.tasklist.usertests",
        features = "classpath:tasklist.features/taskmanagement.feature"
)


public class RunCukesIT {
//Old plugin plugin={"pretty","html:target/cucumber"},
    @AfterClass
    public static void setup() {
        Reporter.loadXMLConfig(new File("src/test/resources/extent-config.xml"));
        Reporter.setSystemInfo("user", System.getProperty("user.name"));
        Reporter.setSystemInfo("os", "Windows");
        Reporter.setTestRunnerOutput("Sample test runner output message");
    }
}


