package com.safebear.tasklist.model;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.time.LocalDate;

/**
 * Created by CCA_Student on 10/07/2018.  Dont worry
 */
public class TaskTest {
    @Test
    public void creation(){
        LocalDate localDate = LocalDate.now();
        Task task = new Task(1L, "Configure Jenkins Server", localDate, false);
        Assertions.assertThat(task.getId()).isEqualTo(1L);

        Assertions.assertThat(task.getName()).isNotBlank();
        Assertions.assertThat(task.getName()).isEqualTo("Configure Jenkins Server");
        Assertions.assertThat(task.getDueDate()).isEqualTo(localDate);
        Assertions.assertThat(task.getCompleted()).isEqualTo(false);
        //The end of the class is here.
    }

}
